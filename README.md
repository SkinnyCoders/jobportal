# JobPortal

API sederhana yang saya buat untuk kebutuhan technical test, Rest API ini saya buat menggunakan Java - Spring Boot dan juga Database mySql, pada awal pengerjaan saya ingin menggunakan noSql yaitu ElasticSearch, namun saya urungkan karena saya rasa teralalu overpower.

## Installation

Clone project lalu jalankan di IDE anda,default port 8080, dan sudah menggunakan swagger2.

```bash
mvn compaile -> java -jar package.class.name --server.port=8500
```

## Database Scema
![alt text](https://gitlab.com/SkinnyCoders/jobportal/-/raw/main/database_scema.png)

## The Controllers
![alt text](https://gitlab.com/SkinnyCoders/jobportal/-/raw/main/controllers.png)

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
