package com.ebdesk;

import javax.annotation.Resource;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import com.ebdesk.services.Impl.UploadService;


@SpringBootApplication
public class RestStudentApplication extends SpringBootServletInitializer{
	
	@Resource
	UploadService servUpload;
	
	public static void main(String[] args) {
		SpringApplication.run(RestStudentApplication.class, args);
	}
	
	public void run(String... arg) throws Exception {
		servUpload.init();
	}


}
