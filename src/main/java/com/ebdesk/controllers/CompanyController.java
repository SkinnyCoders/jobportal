package com.ebdesk.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.ebdesk.models.CompanyDetail;
import com.ebdesk.models.JobModel;
import com.ebdesk.models.MessageModel;
import com.ebdesk.models.ProposalModel;
import com.ebdesk.models.Users;
import com.ebdesk.services.Impl.CompanyServiceImpl;

@Controller
@RequestMapping(value = "/company")
public class CompanyController {
	
	@Autowired
	CompanyServiceImpl servImpl;
	
	@PostMapping("add-detail")
	public ResponseEntity<?> addDetail(@RequestBody CompanyDetail detail){
		MessageModel msg = new MessageModel();
		
		String result = servImpl.addDetail(detail);
		
		if (result.equals("OK")) {
			msg.setStatus(true);
			msg.setMessage("suceess");
			return new ResponseEntity<>(msg, HttpStatus.CREATED) ;
		}else {
			msg.setStatus(false);
			msg.setMessage("failed");
			return new ResponseEntity<>(msg, HttpStatus.INTERNAL_SERVER_ERROR) ;
		}
	}
	
	@PostMapping("add-jobs")
	public ResponseEntity<?> addJobs(@RequestBody JobModel job){
		MessageModel msg = new MessageModel();
		
		String result = servImpl.addJob(job);
		
		if (result.equals("OK")) {
			msg.setStatus(true);
			msg.setMessage("suceess");
			return new ResponseEntity<>(msg, HttpStatus.CREATED) ;
		}else {
			msg.setStatus(false);
			msg.setMessage("failed");
			return new ResponseEntity<>(msg, HttpStatus.INTERNAL_SERVER_ERROR) ;
		}
	}
	
	@GetMapping("/all-jobs")
	public ResponseEntity<?> getAllUsers(@RequestParam int id_user){
		MessageModel msg = new MessageModel();
		
		List<JobModel> JobsData = new ArrayList<JobModel>();
		servImpl.listJobs(id_user).forEach(JobsData::add);
		
		Map<String, Object> datas = new HashMap<String, Object>();
		datas.put("datas", JobsData);
		datas.put("total", JobsData.size());
		
		msg.setStatus(true);
		msg.setMessage("Success Get Data");
		msg.setData(datas);
		
		
		if (JobsData.isEmpty()) {
			msg.setStatus(false);
			msg.setMessage("Data Not Found");
			return new ResponseEntity<MessageModel>(msg, HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<>(msg, HttpStatus.OK) ;
	}
	
	@GetMapping("/proposal")
	public ResponseEntity<?> getProposal(@RequestParam int id_user, @RequestParam int id_job){
		MessageModel msg = new MessageModel();
		
		try {
			ProposalModel data_student = servImpl.getProposal(id_user, id_job);
			msg.setData(data_student);
			msg.setStatus(true);
			msg.setMessage("success");
			return new ResponseEntity<MessageModel>(msg, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			msg.setStatus(false);
			msg.setMessage("failed");
			return new ResponseEntity<MessageModel>(msg, HttpStatus.NOT_FOUND);
		}
	}

}
