package com.ebdesk.controllers;

import java.net.Authenticator.RequestorType;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.ebdesk.models.CompanyDetail;
import com.ebdesk.models.FreelanceDetail;
import com.ebdesk.models.JobModel;
import com.ebdesk.models.MessageModel;
import com.ebdesk.models.ProposalModel;
import com.ebdesk.services.Impl.FreelanceServiceImpl;

@Controller
@RequestMapping(value = "/freelancer")
public class FreelancerController {
	@Autowired
	FreelanceServiceImpl servImpl;
	
	@RequestMapping(path = "/add-proposal",method =RequestMethod.POST,
			consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE}, 
			produces = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<?> addProposal(@RequestBody ProposalModel proposal, @RequestParam(value = "file", required = false) MultipartFile file){

		MessageModel msg = new MessageModel();
		
		String proposalresult = servImpl.addProposal(proposal, file);
		
		if(proposalresult.equals("failed")) {
			msg.setStatus(false);
			msg.setMessage("Your has been apply");
			return new ResponseEntity<>(msg, HttpStatus.FORBIDDEN);
		}else if (proposalresult.equals("OK")) {
			msg.setStatus(true);
			msg.setMessage("suceess");
			return new ResponseEntity<>(msg, HttpStatus.CREATED);
		}else {
			msg.setStatus(false);
			msg.setMessage("failed");
			return new ResponseEntity<>(msg, HttpStatus.INTERNAL_SERVER_ERROR) ;
		}
	}
	
	@PostMapping("add-detail")
	public ResponseEntity<?> addDetail(@RequestBody FreelanceDetail detail){
		MessageModel msg = new MessageModel();
		
		String result = servImpl.addDetail(detail);
		
		if (result.equals("OK")) {
			msg.setStatus(true);
			msg.setMessage("suceess");
			return new ResponseEntity<>(msg, HttpStatus.CREATED) ;
		}else {
			msg.setStatus(false);
			msg.setMessage("failed");
			return new ResponseEntity<>(msg, HttpStatus.INTERNAL_SERVER_ERROR) ;
		}
	}
	
	@GetMapping("/all-jobs")
	public ResponseEntity<?> getAllJobs(){
		MessageModel msg = new MessageModel();
		
		List<JobModel> JobsData = new ArrayList<JobModel>();
		servImpl.listjobs().forEach(JobsData::add);
		
		Map<String, Object> datas = new HashMap<String, Object>();
		datas.put("datas", JobsData);
		datas.put("total", JobsData.size());
		
		msg.setStatus(true);
		msg.setMessage("Success Get Data");
		msg.setData(datas);
		
		
		if (JobsData.isEmpty()) {
			msg.setStatus(false);
			msg.setMessage("Data Not Found");
			return new ResponseEntity<MessageModel>(msg, HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<>(msg, HttpStatus.OK) ;
	}
	
	@GetMapping("/detail-job")
	public ResponseEntity<?> getDetailJob(@RequestParam int id_job){
		MessageModel msg = new MessageModel();
		
		try {
			JobModel data_job = servImpl.detailJob(id_job);
			msg.setData(data_job);
			msg.setStatus(true);
			msg.setMessage("success");
			return new ResponseEntity<MessageModel>(msg, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			msg.setStatus(false);
			msg.setMessage("failed");
			return new ResponseEntity<MessageModel>(msg, HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("/search-job-title")
	public ResponseEntity<?> searchJobByTitle(@RequestParam String title){
		MessageModel msg = new MessageModel();
		
		try {
			List<JobModel> data_job = servImpl.searchJobByTitle(title);
			msg.setData(data_job);
			msg.setStatus(true);
			msg.setMessage("success");
			return new ResponseEntity<MessageModel>(msg, HttpStatus.OK);
		} catch (Exception e) {
			// TODO: handle exception
			msg.setStatus(false);
			msg.setMessage("failed");
			return new ResponseEntity<MessageModel>(msg, HttpStatus.NOT_FOUND);
		}
	}
}
