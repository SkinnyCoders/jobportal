package com.ebdesk.controllers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.ebdesk.models.MessageModel;
import com.ebdesk.models.UserLogin;
import com.ebdesk.models.Users;
import com.ebdesk.services.Impl.UsersServiceImpl;

@Controller
@RequestMapping(value = "/users")
public class UsersController {
	@Autowired
	UsersServiceImpl servImpl;
	
	@PostMapping("/add-users")
	public ResponseEntity<?> addStudent(@RequestBody Users users){
		MessageModel msg = new MessageModel();
		
		String result = servImpl.addUsers(users);
		
		if(result.equals("username exist")) {
			msg.setStatus(false);
			msg.setMessage("failed, Username already exist");
			return new ResponseEntity<>(msg, HttpStatus.CONFLICT) ;
		}else if (result.equals("OK")) {
			msg.setStatus(true);
			msg.setMessage("suceess");
			return new ResponseEntity<>(msg, HttpStatus.CREATED) ;
		}else {
			msg.setStatus(false);
			msg.setMessage("failed");
			return new ResponseEntity<>(msg, HttpStatus.INTERNAL_SERVER_ERROR) ;
		}
	}
	
	@PostMapping("/login-users")
	public ResponseEntity<?> loginUser(@RequestBody UserLogin users){
		MessageModel msg = new MessageModel();
		
		try {
			String id_user = servImpl.loginCheck(users);
			
			if(id_user != null) {
				msg.setStatus(true);
				msg.setMessage("suceess");
				return new ResponseEntity<>(msg, HttpStatus.ACCEPTED) ;
			}else {
				msg.setStatus(false);
				msg.setMessage("Login Failed");
				return new ResponseEntity<>(msg, HttpStatus.FORBIDDEN) ;
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			msg.setStatus(false);
			msg.setMessage("failed");
			return new ResponseEntity<>(msg, HttpStatus.INTERNAL_SERVER_ERROR) ;
		}
	}
	
	@GetMapping("/all-users")
	public ResponseEntity<?> getAllUsers(){
		MessageModel msg = new MessageModel();
		
		List<Users> UserData = new ArrayList<Users>();
		servImpl.getAllUser().forEach(UserData::add);
		
		Map<String, Object> datas = new HashMap<String, Object>();
		datas.put("datas", UserData);
		datas.put("total", UserData.size());
		
		msg.setStatus(true);
		msg.setMessage("Success Get Data");
		msg.setData(datas);
		
		
		if (UserData.isEmpty()) {
			msg.setStatus(false);
			msg.setMessage("Data Not Found");
			return new ResponseEntity<MessageModel>(msg, HttpStatus.NO_CONTENT);
		}

		return new ResponseEntity<>(msg, HttpStatus.OK) ;
	}
	
}
