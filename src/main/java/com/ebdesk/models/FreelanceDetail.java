package com.ebdesk.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name= "user_detail")
public class FreelanceDetail {
	@Id
	@ApiModelProperty(value = "id_user_detail", hidden = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_user_detail")
	private int id_user_detail;
	
	@Column(name = "id_user")
	private int id_user;

	@Column(name = "full_name")
	private String full_name;
	
	@Column(name = "address")
	private String address;
	
	@Column(name = "birth_date")
	private String birth_date;
	
	@Column(name = "gender")
	private String gender;
	
	@Column(name = "biography")
	private String biography;
	
	@Column(name = "link_portopolio")
	private String link_portopolio;
	
	@Column(name = "skills")
	private String skills;

	public int getId_user_detail() {
		return id_user_detail;
	}

	public void setId_user_detail(int id_user_detail) {
		this.id_user_detail = id_user_detail;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	public String getFull_name() {
		return full_name;
	}

	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBirth_date() {
		return birth_date;
	}

	public void setBirth_date(String birth_date) {
		this.birth_date = birth_date;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getBiography() {
		return biography;
	}

	public void setBiography(String biography) {
		this.biography = biography;
	}

	public String getLink_portopolio() {
		return link_portopolio;
	}

	public void setLink_portopolio(String link_portopolio) {
		this.link_portopolio = link_portopolio;
	}

	public String getSkills() {
		return skills;
	}

	public void setSkills(String skills) {
		this.skills = skills;
	}
	
	
	
	
	

}
