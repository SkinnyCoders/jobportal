package com.ebdesk.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name= "jobs")
public class JobModel {
	
	@Id
	@ApiModelProperty(value = "id_job", hidden = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_job")
	private int id_job;
	
	@Column(name = "id_user")
	private int id_user;
	
	@Column(name = "job_title")
	private String job_title;
	
	@Column(name = "position_level")
	private String position_level;
	
	@Column(name = "location")
	private String location;
	
	@Column(name = "year_experient")
	private int experient;
	
	@Column(name = "job_specialisation")
	private String job_specialisation;
	
	@Column(name = "budget_salary")
	private String budget_salary;
	
	@Column(name = "job_description")
	private String job_description;
	
	@Column(name = "job_type")
	private String job_type;
	
	@Column(name = "status")
	private String status;
	
	@ApiModelProperty(value = "created_at", hidden = true)
	@Column(name = "created_at")
	private String created_at;
	
	public int getId_job() {
		return id_job;
	}
	public void setId_job(int id_job) {
		this.id_job = id_job;
	}
	public int getId_user() {
		return id_user;
	}
	public void setId_user(int id_user) {
		this.id_user = id_user;
	}
	public String getJob_title() {
		return job_title;
	}
	public void setJob_title(String job_title) {
		this.job_title = job_title;
	}
	public String getPosition_level() {
		return position_level;
	}
	public void setPosition_level(String position_level) {
		this.position_level = position_level;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public int getExperient() {
		return experient;
	}
	public void setExperient(int experient) {
		this.experient = experient;
	}
	public String getJob_specialisation() {
		return job_specialisation;
	}
	public void setJob_specialisation(String job_specialisation) {
		this.job_specialisation = job_specialisation;
	}
	public String getBudget_salary() {
		return budget_salary;
	}
	public void setBudget_salary(String budget_salary) {
		this.budget_salary = budget_salary;
	}
	public String getJob_description() {
		return job_description;
	}
	public void setJob_description(String job_description) {
		this.job_description = job_description;
	}
	public String getJob_type() {
		return job_type;
	}
	public void setJob_type(String job_type) {
		this.job_type = job_type;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}

}
