package com.ebdesk.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name= "company_detail")
public class ProposalModel {
	@Id
	@ApiModelProperty(value = "id_proposal", hidden = true)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_proposal")
	private int id_proposal;
	
	@Column(name = "id_jobs")
	private int id_jobs;
	
	@Column(name = "id_user")
	private int id_user;

	@ApiModelProperty(value = "resume", hidden = true)
	@Column(name = "resume")
	private String resume;
	
	@Column(name = "application_letter")
	private String application_letter;
	
	@ApiModelProperty(value = "created_at", hidden = true)
	@Column(name = "created_at")
	private String created_at;

	public int getId_proposal() {
		return id_proposal;
	}

	public void setId_proposal(int id_proposal) {
		this.id_proposal = id_proposal;
	}

	public int getId_jobs() {
		return id_jobs;
	}

	public void setId_jobs(int id_jobs) {
		this.id_jobs = id_jobs;
	}

	public int getId_user() {
		return id_user;
	}

	public void setId_user(int id_user) {
		this.id_user = id_user;
	}

	public String getResume() {
		return resume;
	}

	public void setResume(String resume) {
		this.resume = resume;
	}

	public String getApplication_letter() {
		return application_letter;
	}

	public void setApplication_letter(String application_letter) {
		this.application_letter = application_letter;
	}

	public String getCreated_at() {
		return created_at;
	}

	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	
	
}
