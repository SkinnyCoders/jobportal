package com.ebdesk.repositories;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ebdesk.models.CompanyDetail;
import com.ebdesk.models.ProposalModel;

public interface CompanyRepo extends CrudRepository<CompanyDetail, Integer> {
	
	@Query(value = "SELECT * FROM `proposal` WHERE proposal.id_user = ?1 AND proposal.id_jobs = ?2 LIMIT 1", nativeQuery = true)
	ProposalModel getProposalData(int id_user, int id_job);
	

}
