package com.ebdesk.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ebdesk.models.FreelanceDetail;
import com.ebdesk.models.JobModel;
import com.ebdesk.models.ProposalModel;

public interface FreelanceRepo extends CrudRepository<FreelanceDetail, Integer>{
	
	@Query(value = "SELECT * FROM `jobs` WHERE 1", nativeQuery = true)
	public List<JobModel> getAllJobs();
	
	@Query(value = "SELECT * FROM `jobs` WHERE id_job = ?1", nativeQuery = true)
	public JobModel getDetailJobs(int id_job);
	
	@Query(value = "SELECT * FROM `proposal` WHERE id_jobs = ?1 AND id_user = ?2", nativeQuery = true)
	public List<ProposalModel> checkproposal(int id_jobs,int id_user);
	
	@Query(value = "SELECT * FROM `jobs` WHERE jobs.job_title LIKE '%?1%'", nativeQuery = true)
	public List<JobModel> searchJobByTitle(String title);

}
