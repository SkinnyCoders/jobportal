package com.ebdesk.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.ebdesk.models.JobModel;

public interface JobRepo extends CrudRepository<JobModel, Integer> {

	@Query(value = "SELECT * FROM `jobs` WHERE jobs.id_user = ?1", nativeQuery = true)
	List<JobModel> getAllJobsCreated(int id_user);
}
