package com.ebdesk.repositories;

import org.springframework.data.repository.CrudRepository;

import com.ebdesk.models.ProposalModel;

public interface ProposalRepo extends CrudRepository<ProposalModel, Integer> {


}
