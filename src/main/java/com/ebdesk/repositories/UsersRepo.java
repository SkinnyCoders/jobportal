package com.ebdesk.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;

import com.ebdesk.models.Users;

@EnableJpaRepositories
public interface UsersRepo extends CrudRepository<Users, Integer> {
	@Query(value = "SELECT id_user FROM `users` WHERE users.username = ?1 AND users.password = ?2", nativeQuery = true)
	String loginCheck(String username, String password);
	
	@Query(value = "SELECT `username` FROM `users` WHERE users.username = ?1", nativeQuery = true)
	String checkUsername(String username);
}
