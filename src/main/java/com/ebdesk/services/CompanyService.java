package com.ebdesk.services;

import java.util.List;

import com.ebdesk.models.CompanyDetail;
import com.ebdesk.models.JobModel;
import com.ebdesk.models.ProposalModel;

public interface CompanyService {

	public String addDetail(CompanyDetail detail);
	
	public String addJob(JobModel job);
	
	public List<JobModel> listJobs(int id_user);
	
	public ProposalModel getProposal(int id_user, int id_job);
}
