package com.ebdesk.services;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.ebdesk.models.FreelanceDetail;
import com.ebdesk.models.JobModel;
import com.ebdesk.models.ProposalModel;

public interface FreelanceService {
	
	public String addDetail(FreelanceDetail detail);

	public String addProposal(ProposalModel proposal, MultipartFile file);
	
	public List<JobModel> listjobs();
	
	public JobModel detailJob(int id_job);
	
	public List<JobModel> searchJobByTitle(String title);
}
