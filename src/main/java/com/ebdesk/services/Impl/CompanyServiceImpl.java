package com.ebdesk.services.Impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ebdesk.models.CompanyDetail;
import com.ebdesk.models.JobModel;
import com.ebdesk.models.ProposalModel;
import com.ebdesk.repositories.CompanyRepo;
import com.ebdesk.repositories.JobRepo;
import com.ebdesk.services.CompanyService;

@Service
@Transactional(noRollbackFor = RuntimeException.class)
public class CompanyServiceImpl implements CompanyService {
	
	@Autowired
	CompanyRepo repo;
	
	@Autowired
	JobRepo repojob;
	

	@Override
	public String addDetail(CompanyDetail detail) {
		// TODO Auto-generated method stub
		String result = "OK";
		try {
			repo.save(detail);
		} catch (Exception e) {
			// TODO: handle exception
			result = "failed "+e.getMessage();
		}
		
		return result;
	}


	@Override
	public String addJob(JobModel job) {
		// TODO Auto-generated method stub
		String result = "OK";
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
		String dateNow = df.format(date);
		
		try {
			job.setCreated_at(dateNow);
			repojob.save(job);
		} catch (Exception e) {
			// TODO: handle exception
			result = "failed "+e.getMessage();
		}
		
		return result;
	}


	@Override
	public List<JobModel> listJobs(int id_user) {
		// TODO Auto-generated method stub
		List<JobModel> jobs = repojob.getAllJobsCreated(id_user);
		
		if(jobs != null) {
			return jobs;
		}else {
			return null;
		}
	}


	@Override
	public ProposalModel getProposal(int id_user, int id_job) {
		// TODO Auto-generated method stub
		return repo.getProposalData(id_user, id_job);
	}
}
