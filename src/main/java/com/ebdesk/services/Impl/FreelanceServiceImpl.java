package com.ebdesk.services.Impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.ebdesk.functions.FileNameReplace;
import com.ebdesk.models.FreelanceDetail;
import com.ebdesk.models.JobModel;
import com.ebdesk.models.ProposalModel;
import com.ebdesk.repositories.FreelanceRepo;
import com.ebdesk.repositories.ProposalRepo;
import com.ebdesk.services.FreelanceService;

@Service
@Transactional
public class FreelanceServiceImpl implements FreelanceService {

	@Autowired
	ProposalRepo proposalRepo;
	
	@Autowired
	FreelanceRepo repo;
	
	@Autowired
	UploadService uploadService;

	@Override
	public String addProposal(ProposalModel proposal, MultipartFile file) {
		// TODO Auto-generated method stub
		List<ProposalModel> checkProposal = repo.checkproposal(proposal.getId_jobs(), proposal.getId_user());
		String result = "OK";
		if(checkProposal.size() > 0) {
			result = "failed";
		}else {
			Date date = new Date();
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			df.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
			String dateNow = df.format(date);
			
			proposal.setCreated_at(dateNow);
			
			if(!file.isEmpty()) {
		
				if(uploadService.save(file)) {
					String[] root_arr = uploadService.root.toString().split("/");
					String path_folder = root_arr[root_arr.length-1];
					proposal.setResume("/"+path_folder+"/"+file.getOriginalFilename());
					
					proposalRepo.save(proposal);
					
				}else {
					result = "failed upload resume";
				}
				
			}else {
				result = "resume is empty";
			}
		}
		
		return result;
	}

	@Override
	public List<JobModel> listjobs() {
		// TODO Auto-generated method stub
		List<JobModel> jobs = repo.getAllJobs();
		
		if(jobs != null) {
			return jobs;
		}else {
			return null;
		}
	}

	@Override
	public String addDetail(FreelanceDetail detail) {
		// TODO Auto-generated method stub
		String result = "OK";
		try {
			repo.save(detail);
		} catch (Exception e) {
			// TODO: handle exception
			result = "failed "+e.getMessage();
		}
		
		return result;
	}

	@Override
	public JobModel detailJob(int id_job) {
		// TODO Auto-generated method stub
		return repo.getDetailJobs(id_job);
	}

	@Override
	public List<JobModel> searchJobByTitle(String title) {
		// TODO Auto-generated method stub
		return repo.searchJobByTitle(title);
	}
	
	
}
