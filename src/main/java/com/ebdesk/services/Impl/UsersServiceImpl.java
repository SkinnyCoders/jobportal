package com.ebdesk.services.Impl;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ebdesk.models.UserLogin;
import com.ebdesk.models.Users;
import com.ebdesk.repositories.UsersRepo;
import com.ebdesk.services.UsersService;

@Service
@Transactional
public class UsersServiceImpl implements UsersService {
	
	@Autowired
	UsersRepo repo;

	@Override
	public String addUsers(Users users) {
		// TODO Auto-generated method stub
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
		String dateNow = df.format(date);
		users.setCreated_at(dateNow);
		
		//cek username
		String usernameCheck = repo.checkUsername(users.getUsername());
		String result = "OK";
		if(usernameCheck != null) {
			result = "username exist";
			
		}else {
			try {
				repo.save(users);
			} catch (Exception e) {
				// TODO: handle exception
				result = "failed";
			}
		}
		
		return result;
	}

	@Override
	public String loginCheck(UserLogin login) throws UnsupportedEncodingException {
		// TODO Auto-generated method stub
		byte[] bytes = login.getPassword().getBytes("UTF-16BE");
		String encoded = Base64.getEncoder().encodeToString(bytes);
		
		return repo.loginCheck(login.getUsername(), encoded);
	}

	@Override
	public List<Users> getAllUser() {
		// TODO Auto-generated method stub
		return (List<Users>) repo.findAll();
	}

}
