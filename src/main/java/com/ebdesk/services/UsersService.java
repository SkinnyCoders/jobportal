package com.ebdesk.services;

import java.io.UnsupportedEncodingException;
import java.util.List;

import com.ebdesk.models.UserLogin;
import com.ebdesk.models.Users;

public interface UsersService {
	public String addUsers(Users users);
	
	public List<Users> getAllUser();
	
	public String loginCheck(UserLogin login) throws UnsupportedEncodingException;
	
}
